<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210126233057 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE region DROP FOREIGN KEY FK_F62F176B40A33C7');
        $this->addSql('DROP INDEX UNIQ_F62F176B40A33C7 ON region');
        $this->addSql('ALTER TABLE region CHANGE president_id delegue_regional_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE region ADD CONSTRAINT FK_F62F176717C8890 FOREIGN KEY (delegue_regional_id) REFERENCES person (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_F62F176717C8890 ON region (delegue_regional_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE region DROP FOREIGN KEY FK_F62F176717C8890');
        $this->addSql('DROP INDEX UNIQ_F62F176717C8890 ON region');
        $this->addSql('ALTER TABLE region CHANGE delegue_regional_id president_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE region ADD CONSTRAINT FK_F62F176B40A33C7 FOREIGN KEY (president_id) REFERENCES person (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_F62F176B40A33C7 ON region (president_id)');
    }
}
