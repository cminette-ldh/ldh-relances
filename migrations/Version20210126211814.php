<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210126211814 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE federation ADD president_id INT DEFAULT NULL, ADD treasurer_id INT DEFAULT NULL, ADD secretary_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE federation ADD CONSTRAINT FK_AD241BCDB40A33C7 FOREIGN KEY (president_id) REFERENCES person (id)');
        $this->addSql('ALTER TABLE federation ADD CONSTRAINT FK_AD241BCD55808438 FOREIGN KEY (treasurer_id) REFERENCES person (id)');
        $this->addSql('ALTER TABLE federation ADD CONSTRAINT FK_AD241BCDA2A63DB2 FOREIGN KEY (secretary_id) REFERENCES person (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_AD241BCDB40A33C7 ON federation (president_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_AD241BCD55808438 ON federation (treasurer_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_AD241BCDA2A63DB2 ON federation (secretary_id)');
        $this->addSql('ALTER TABLE region ADD president_id INT DEFAULT NULL, ADD treasurer_id INT DEFAULT NULL, ADD secretary_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE region ADD CONSTRAINT FK_F62F176B40A33C7 FOREIGN KEY (president_id) REFERENCES person (id)');
        $this->addSql('ALTER TABLE region ADD CONSTRAINT FK_F62F17655808438 FOREIGN KEY (treasurer_id) REFERENCES person (id)');
        $this->addSql('ALTER TABLE region ADD CONSTRAINT FK_F62F176A2A63DB2 FOREIGN KEY (secretary_id) REFERENCES person (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_F62F176B40A33C7 ON region (president_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_F62F17655808438 ON region (treasurer_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_F62F176A2A63DB2 ON region (secretary_id)');
        $this->addSql('ALTER TABLE section ADD president_id INT DEFAULT NULL, ADD treasurer_id INT DEFAULT NULL, ADD secretary_id INT DEFAULT NULL, DROP structure');
        $this->addSql('ALTER TABLE section ADD CONSTRAINT FK_2D737AEFB40A33C7 FOREIGN KEY (president_id) REFERENCES person (id)');
        $this->addSql('ALTER TABLE section ADD CONSTRAINT FK_2D737AEF55808438 FOREIGN KEY (treasurer_id) REFERENCES person (id)');
        $this->addSql('ALTER TABLE section ADD CONSTRAINT FK_2D737AEFA2A63DB2 FOREIGN KEY (secretary_id) REFERENCES person (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_2D737AEFB40A33C7 ON section (president_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_2D737AEF55808438 ON section (treasurer_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_2D737AEFA2A63DB2 ON section (secretary_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE federation DROP FOREIGN KEY FK_AD241BCDB40A33C7');
        $this->addSql('ALTER TABLE federation DROP FOREIGN KEY FK_AD241BCD55808438');
        $this->addSql('ALTER TABLE federation DROP FOREIGN KEY FK_AD241BCDA2A63DB2');
        $this->addSql('DROP INDEX UNIQ_AD241BCDB40A33C7 ON federation');
        $this->addSql('DROP INDEX UNIQ_AD241BCD55808438 ON federation');
        $this->addSql('DROP INDEX UNIQ_AD241BCDA2A63DB2 ON federation');
        $this->addSql('ALTER TABLE federation DROP president_id, DROP treasurer_id, DROP secretary_id');
        $this->addSql('ALTER TABLE region DROP FOREIGN KEY FK_F62F176B40A33C7');
        $this->addSql('ALTER TABLE region DROP FOREIGN KEY FK_F62F17655808438');
        $this->addSql('ALTER TABLE region DROP FOREIGN KEY FK_F62F176A2A63DB2');
        $this->addSql('DROP INDEX UNIQ_F62F176B40A33C7 ON region');
        $this->addSql('DROP INDEX UNIQ_F62F17655808438 ON region');
        $this->addSql('DROP INDEX UNIQ_F62F176A2A63DB2 ON region');
        $this->addSql('ALTER TABLE region DROP president_id, DROP treasurer_id, DROP secretary_id');
        $this->addSql('ALTER TABLE section DROP FOREIGN KEY FK_2D737AEFB40A33C7');
        $this->addSql('ALTER TABLE section DROP FOREIGN KEY FK_2D737AEF55808438');
        $this->addSql('ALTER TABLE section DROP FOREIGN KEY FK_2D737AEFA2A63DB2');
        $this->addSql('DROP INDEX UNIQ_2D737AEFB40A33C7 ON section');
        $this->addSql('DROP INDEX UNIQ_2D737AEF55808438 ON section');
        $this->addSql('DROP INDEX UNIQ_2D737AEFA2A63DB2 ON section');
        $this->addSql('ALTER TABLE section ADD structure VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, DROP president_id, DROP treasurer_id, DROP secretary_id');
    }
}
