<?php

namespace App\Repository;

use App\Entity\Federation;
use App\Entity\Section;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Federation|null find($id, $lockMode = null, $lockVersion = null)
 * @method Federation|null findOneBy(array $criteria, array $orderBy = null)
 * @method Federation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FederationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Federation::class);
    }

    public function findAll()
    {
        return $this->findBy(array(), array('libelle' => 'ASC'));
    }

    public function findOneByNoClient($noClient): ?Federation
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.noClient = :val')
            ->setParameter('val', $noClient)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function findOneByNoClientOrName($string): ?Federation
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.noClient = :val')
            ->orWhere('s.libelle = :val')
            ->setParameter('val', $string)
            ->getQuery()
            ->getOneOrNullResult();
    }
}
