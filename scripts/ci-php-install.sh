#!/bin/bash

# We need to install dependencies only for Docker
[[ ! -e /.dockerenv ]] && exit 0

set -xe

apt-get update \
&& apt-get install -y \
&& apt-get autoremove -y \
&& apt-get install -y curl git zip zlib1g libpng-* \
&& apt-get install -y zlib1g-dev libpq-dev libicu-dev libxml2-dev libzip-dev -y \
&& docker-php-ext-install intl mysqli pdo pdo_mysql gd zip xml \
&& curl -sS https://get.symfony.com/cli/installer | bash \
&& mv /root/.symfony/bin/symfony /usr/local/bin/symfony \
&& curl -sS https://getcomposer.org/installer | php

cp .env.test .env
