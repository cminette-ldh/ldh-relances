<?php

namespace App\Repository;

use App\Entity\Section;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\DBAL\Types\Types;

/**
 * @method Section|null find($id, $lockMode = null, $lockVersion = null)
 * @method Section|null findOneBy(array $criteria, array $orderBy = null)
 * @method Section[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SectionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Section::class);
    }

    public function findAll()
    {
        return $this->findBy(array(), array('libelle' => 'ASC'));
    }

    public function findOneByNoClient($noClient): ?Section
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.noClient = :val')
            ->setParameter('val', $noClient)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function findOneByNoClientOrName($string): ?Section
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.noClient = :val')
            ->orWhere('s.libelle = :val')
            ->setParameter('val', trim($string))
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function findByMissingNoClient($noClients)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.noClient NOT IN (:noClient)')
            ->setParameter('noClient', $noClients)
            ->leftJoin('s.federation', 'federation')
            ->leftJoin('federation.region', 'region')
            ->OrderBy('region.libelle,s.libelle', 'ASC')
            ->getQuery()
            ->getResult();
    }
}
