<?php

namespace App\Entity;

use App\Repository\SectionRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=SectionRepository::class)
 */
class Section
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=7)
     */
    private $noClient;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $compte;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $libelle;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $preferedEmailaddress;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $address;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $zipCode;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $town;

    /**
     * @ORM\OneToOne(targetEntity=Person::class, inversedBy="sectionPresident", cascade={"persist", "remove"})
     */
    private $president;

    /**
     * @ORM\OneToOne(targetEntity=Person::class, inversedBy="sectionTreasurer", cascade={"persist", "remove"})
     */
    private $treasurer;

    /**
     * @ORM\OneToOne(targetEntity=Person::class, inversedBy="sectionSecretary",cascade={"persist", "remove"})
     */
    private $secretary;

    /**
     * @ORM\ManyToOne(targetEntity=Federation::class, inversedBy="sections")
     */
    private $federation;

    public function __construct()
    {
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNoClient(): ?string
    {
        return $this->noClient;
    }

    public function setNoClient(string $noClient): self
    {
        $this->noClient = $noClient;

        return $this;
    }

    public function getCompte(): ?string
    {
        return $this->compte;
    }

    public function setCompte(?string $compte): self
    {
        $this->compte = $compte;

        return $this;
    }

    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    public function setLibelle(string $libelle): self
    {
        $this->libelle = $libelle;

        return $this;
    }

    public function getPreferedEmailaddress(): ?string
    {
        return $this->preferedEmailaddress;
    }

    public function setPreferedEmailaddress(?string $preferedEmailaddress): self
    {
        $this->preferedEmailaddress = $preferedEmailaddress;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(?string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getZipCode(): ?string
    {
        return $this->zipCode;
    }

    public function setZipCode(?string $zipCode): self
    {
        $this->zipCode = $zipCode;

        return $this;
    }

    public function getTown(): ?string
    {
        return $this->town;
    }

    public function setTown(?string $town): self
    {
        $this->town = $town;

        return $this;
    }

    public function getPresident(): ?Person
    {
        return $this->president;
    }

    public function setPresident(?Person $president): self
    {
        $this->president = $president;

        return $this;
    }

    public function getTreasurer(): ?Person
    {
        return $this->treasurer;
    }

    public function setTreasurer(?Person $treasurer): self
    {
        $this->treasurer = $treasurer;

        return $this;
    }

    public function getSecretary(): ?Person
    {
        return $this->secretary;
    }

    public function setSecretary(?Person $secretary): self
    {
        $this->secretary = $secretary;

        return $this;
    }

    public function getFederation(): ?Federation
    {
        return $this->federation;
    }

    public function setFederation(?Federation $federation): self
    {
        $this->federation = $federation;

        return $this;
    }
}
