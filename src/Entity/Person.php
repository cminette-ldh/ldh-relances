<?php

namespace App\Entity;

use App\Repository\PersonRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PersonRepository::class)
 */
class Person
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $firstName;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $lastName;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $telephoneHome;

    /**
     * @ORM\OneToOne(targetEntity=Section::class, mappedBy="president", cascade={"persist", "remove"})
     */
    private $sectionPresident;

    /**
     * @ORM\OneToOne(targetEntity=Federation::class, mappedBy="president", cascade={"persist", "remove"})
     */
    private $federationPresident;

    /**
     * @ORM\OneToOne(targetEntity=Section::class, mappedBy="treasurer", cascade={"persist", "remove"})
     */
    private $sectionTreasurer;

    /**
     * @ORM\OneToOne(targetEntity=Region::class, mappedBy="treasurer", cascade={"persist", "remove"})
     */
    private $regionTreasurer;

    /**
     * @ORM\OneToOne(targetEntity=Federation::class, mappedBy="treasurer", cascade={"persist", "remove"})
     */
    private $federationTreasurer;

    /**
     * @ORM\OneToOne(targetEntity=Section::class, mappedBy="secretary", cascade={"persist", "remove"})
     */
    private $sectionSecretary;

    /**
     * @ORM\OneToOne(targetEntity=Region::class, mappedBy="secretary", cascade={"persist", "remove"})
     */
    private $regionSecretary;

    /**
     * @ORM\OneToOne(targetEntity=Federation::class, mappedBy="secretary", cascade={"persist", "remove"})
     */
    private $federationSecretary;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getTelephoneHome(): ?string
    {
        return $this->telephoneHome;
    }

    public function setTelephoneHome(?string $telephoneHome): self
    {
        $this->telephoneHome = $telephoneHome;

        return $this;
    }

    public function getPresident(): ?Section
    {
        return $this->president;
    }

    public function setPresident(?Section $president): self
    {
        // unset the owning side of the relation if necessary
        if ($president === null && $this->president !== null) {
            $this->president->setPresident(null);
        }

        // set the owning side of the relation if necessary
        if ($president !== null && $president->getPresident() !== $this) {
            $president->setPresident($this);
        }

        $this->president = $president;

        return $this;
    }
}
