<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use App\Form\ImportSectionType;
use PhpOffice\PhpSpreadsheet;
use App\Entity\Section;
use App\Entity\Region;
use App\Entity\Federation;
use Doctrine\ORM\EntityManagerInterface;

class SectionController extends AbstractController
{
    /**
     * @Route("/section/list")
     */
    public function list(Request $request)
    {
        $sections = $this->getDoctrine()
            ->getRepository(Section::class)
            ->findAll();

        return $this->render(
            'section/list.html.twig',
            [
            'sections' => $sections,
            'count' => count($sections),
            ]
        );
    }

    /**
     * @Route("/region/list")
     */
    public function regionList(Request $request)
    {
        $sections = $this->getDoctrine()
            ->getRepository(Region::class)
            ->findAll();

        return $this->render(
            'section/list.html.twig',
            [
            'sections' => $sections,
            'count' => count($sections),
            ]
        );
    }

    /**
     * @Route("/federation/list")
     */
    public function federationList(Request $request)
    {
        $sections = $this->getDoctrine()
            ->getRepository(Federation::class)
            ->findAll();

        return $this->render(
            'section/list.html.twig',
            [
            'sections' => $sections,
            'count' => count($sections),
            ]
        );
    }

    /**
     * @Route("/section/import")
     */
    public function import(Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();

        $form = $this->createForm(ImportSectionType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /**
*
             *
 * @var UploadedFile $file
*/
            $uploadedFile = $form->get('file')->getData();

            if ($uploadedFile) {
                try {
                    $spreadsheet = PhpSpreadsheet\IOFactory::load($uploadedFile);
                    $rows = $spreadsheet->getActiveSheet()->toArray();

                    unset($rows[0]);

                    foreach ($rows as $row) {
                        if ($row[0] == 'Section') {
                            $section = $this->getDoctrine()
                                ->getRepository(Section::class)
                                ->findOneByNoClient($row[1]);

                            if ($section == null) {
                                $section = new Section();
                            }

                            $section->setNoClient($row[1]);
                            $section->setCompte($row[2]);
                            $section->SetLibelle($row[3]);
                            $section->SetPreferedEmailaddress($row[4]);
                            $section->SetAddress($row[5]);
                            $section->SetZipCode($row[6]);
                            $section->SetTown($row[7]);

                            $entityManager->persist($section);
                        }

                        if ($row[0] == 'Comité Régional') {
                            $region = $this->getDoctrine()
                                ->getRepository(Region::class)
                                ->findOneByNoClient($row[1]);

                            if ($region == null) {
                                $region = new Region();
                            }

                            $region->setNoClient($row[1]);
                            $region->setCompte($row[2]);
                            $region->SetLibelle($row[3]);
                            $region->SetPreferedEmailaddress($row[4]);
                            $region->SetAddress($row[5]);
                            $region->SetZipCode($row[6]);
                            $region->SetTown($row[7]);

                            $entityManager->persist($region);
                        }

                        if ($row[0] == 'Fédération') {
                            $federation = $this->getDoctrine()
                                ->getRepository(Federation::class)
                                ->findOneByNoClient($row[1]);

                            if ($federation == null) {
                                $federation = new Federation();
                            }

                            $federation->setNoClient($row[1]);
                            $federation->setCompte($row[2]);
                            $federation->SetLibelle($row[3]);
                            $federation->SetPreferedEmailaddress($row[4]);
                            $federation->SetAddress($row[5]);
                            $federation->SetZipCode($row[6]);
                            $federation->SetTown($row[7]);

                            $entityManager->persist($federation);
                        }
                    }

                    $entityManager->flush();

                    return $this->redirectToRoute("app_section_list");
                } catch (FileException $e) {
                    // ... handle exception if something happens during file upload
                }
            }
        }

        return $this->render(
            'section/import.html.twig',
            [
            'form' => $form->createView(),
            ]
        );
    }
}
