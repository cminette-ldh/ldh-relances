<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210126212105 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE federation ADD region_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE federation ADD CONSTRAINT FK_AD241BCD98260155 FOREIGN KEY (region_id) REFERENCES region (id)');
        $this->addSql('CREATE INDEX IDX_AD241BCD98260155 ON federation (region_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE federation DROP FOREIGN KEY FK_AD241BCD98260155');
        $this->addSql('DROP INDEX IDX_AD241BCD98260155 ON federation');
        $this->addSql('ALTER TABLE federation DROP region_id');
    }
}
