<?php

namespace App\Entity;

use App\Repository\RegionRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=RegionRepository::class)
 */
class Region
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $libelle;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $noClient;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $compte;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $preferedEmailaddress;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $address;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $zipCode;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $town;

    /**
     * @ORM\OneToOne(targetEntity=Person::class, inversedBy="regionTreasurer", cascade={"persist", "remove"})
     */
    private $treasurer;

    /**
     * @ORM\OneToOne(targetEntity=Person::class, inversedBy="regionSecretary",cascade={"persist", "remove"})
     */
    private $secretary;

    /**
     * @ORM\OneToMany(targetEntity=Federation::class, mappedBy="region")
     */
    private $federations;

    /**
     * @ORM\OneToOne(targetEntity=Person::class, cascade={"persist", "remove"})
     */
    private $delegueRegional;

    public function __construct()
    {
        $this->federations = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    public function setLibelle(string $libelle): self
    {
        $this->libelle = $libelle;

        return $this;
    }

    public function getNoClient(): ?string
    {
        return $this->noClient;
    }

    public function setNoClient(string $noClient): self
    {
        $this->noClient = $noClient;

        return $this;
    }

    public function getCompte(): ?string
    {
        return $this->compte;
    }

    public function setCompte(?string $compte): self
    {
        $this->compte = $compte;

        return $this;
    }

    public function getPreferedEmailaddress(): ?string
    {
        return $this->preferedEmailaddress;
    }

    public function setPreferedEmailaddress(?string $preferedEmailaddress): self
    {
        $this->preferedEmailaddress = $preferedEmailaddress;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(?string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getZipCode(): ?string
    {
        return $this->zipCode;
    }

    public function setZipCode(?string $zipCode): self
    {
        $this->zipCode = $zipCode;

        return $this;
    }

    public function getTown(): ?string
    {
        return $this->town;
    }

    public function setTown(?string $town): self
    {
        $this->town = $town;

        return $this;
    }

    public function getTreasurer(): ?Person
    {
        return $this->treasurer;
    }

    public function setTreasurer(?Person $treasurer): self
    {
        $this->treasurer = $treasurer;

        return $this;
    }

    public function getSecretary(): ?Person
    {
        return $this->secretary;
    }

    public function setSecretary(?Person $secretary): self
    {
        $this->secretary = $secretary;

        return $this;
    }

    /**
     * @return Collection|Federation[]
     */
    public function getFederations(): Collection
    {
        return $this->federations;
    }

    public function addFederation(Federation $federation): self
    {
        if (!$this->federations->contains($federation)) {
            $this->federations[] = $federation;
            $federation->setRegion($this);
        }

        return $this;
    }

    public function removeFederation(Federation $federation): self
    {
        if ($this->federations->removeElement($federation)) {
            // set the owning side to null (unless already changed)
            if ($federation->getRegion() === $this) {
                $federation->setRegion(null);
            }
        }

        return $this;
    }

    public function getDelegueRegional(): ?Person
    {
        return $this->delegueRegional;
    }

    public function setDelegueRegional(?Person $delegueRegional): self
    {
        $this->delegueRegional = $delegueRegional;

        return $this;
    }
}
