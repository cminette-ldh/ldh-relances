<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210126232312 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE section ADD CONSTRAINT FK_2D737AEFB40A33C7 FOREIGN KEY (president_id) REFERENCES person (id)');
        $this->addSql('ALTER TABLE section ADD CONSTRAINT FK_2D737AEF55808438 FOREIGN KEY (treasurer_id) REFERENCES person (id)');
        $this->addSql('ALTER TABLE section ADD CONSTRAINT FK_2D737AEFA2A63DB2 FOREIGN KEY (secretary_id) REFERENCES person (id)');
        $this->addSql('ALTER TABLE section ADD CONSTRAINT FK_2D737AEF6A03EFC5 FOREIGN KEY (federation_id) REFERENCES federation (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_2D737AEFB40A33C7 ON section (president_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_2D737AEF55808438 ON section (treasurer_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_2D737AEFA2A63DB2 ON section (secretary_id)');
        $this->addSql('CREATE INDEX IDX_2D737AEF6A03EFC5 ON section (federation_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE section DROP FOREIGN KEY FK_2D737AEFB40A33C7');
        $this->addSql('ALTER TABLE section DROP FOREIGN KEY FK_2D737AEF55808438');
        $this->addSql('ALTER TABLE section DROP FOREIGN KEY FK_2D737AEFA2A63DB2');
        $this->addSql('ALTER TABLE section DROP FOREIGN KEY FK_2D737AEF6A03EFC5');
        $this->addSql('DROP INDEX UNIQ_2D737AEFB40A33C7 ON section');
        $this->addSql('DROP INDEX UNIQ_2D737AEF55808438 ON section');
        $this->addSql('DROP INDEX UNIQ_2D737AEFA2A63DB2 ON section');
        $this->addSql('DROP INDEX IDX_2D737AEF6A03EFC5 ON section');
    }
}
