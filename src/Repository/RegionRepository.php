<?php

namespace App\Repository;

use App\Entity\Region;
use App\Entity\Section;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Region|null find($id, $lockMode = null, $lockVersion = null)
 * @method Region|null findOneBy(array $criteria, array $orderBy = null)
 * @method Region[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RegionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Region::class);
    }

    public function findAll()
    {
        return $this->findBy(array(), array('libelle' => 'ASC'));
    }

    public function findOneByNoClient($noClient): ?Region
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.noClient = :val')
            ->setParameter('val', $noClient)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function findOneByNoClientOrName($string): ?Region
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.noClient = :val')
            ->orWhere('s.libelle = :val')
            ->setParameter('val', $string)
            ->getQuery()
            ->getOneOrNullResult();
    }
}
