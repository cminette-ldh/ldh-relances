<?php

namespace App\Controller;

use App\Entity\Federation;
use App\Entity\Person;
use App\Entity\Region;
use App\Entity\Section;
use App\Form\ImportCountType;
use App\Form\ImportSectionType;
use PhpOffice\PhpSpreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;

class CountController extends AbstractController
{
    /**
     * @Route("/")
     */
    public function default(Request $request) //, SluggerInterface $slugger)
    {
        return $this->redirectToRoute("app_count_list");
    }

    /**
     * @Route("/count")
     */
    public function home(Request $request) //, SluggerInterface $slugger)
    {
        return $this->redirectToRoute("app_count_list");
    }

    /**
     * @Route("/count/list")
     */
    public function list(Request $request)
    {
        $answeredSections = $this->get('session')->get('results');

        if ($answeredSections == null) {
            return $this->redirectToRoute("app_count_import");
        }

        $sections = $this->getDoctrine()
            ->getRepository(Section::class)
            ->findByMissingNoClient($answeredSections);

        return $this->render(
            'count/list.html.twig',
            [
            'sections' => $sections,
            'missingCount' => count($sections),
            'answeredCount' => count($answeredSections)
            ]
        );
    }

    /**
     * @Route("/count/all")
     */
    public function all(Request $request)
    {
        $answeredSections = $this->get('session')->get('results');

        if ($answeredSections == null) {
            return $this->redirectToRoute("app_count_import");
        }

        $nbMissingAnswers =$this->getDoctrine()
            ->getRepository(Section::class)
            ->findByMissingNoClientAndStructure($answeredSections, ['Section','Comité Régional']);

        $sections = $this->getDoctrine()
            ->getRepository(Section::class)
            ->findByStructure(['Section','Comité Régional']);

        return $this->render(
            'count/all.html.twig',
            [
            'sections' => $sections,
            'missingCount' => count($nbMissingAnswers),
            'answeredCount' => count($answeredSections),
            'answeredList' => $answeredSections
            ]
        );
    }

    /**
     * @Route("/count/import")
     */
    public function import(Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();

        $form = $this->createForm(ImportCountType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /**
            *
            *
            * @var UploadedFile $file
            */
            $uploadedFile = $form->get('file')->getData();

            if ($uploadedFile) {
                try {
                    $spreadsheet = PhpSpreadsheet\IOFactory::load($uploadedFile);
                    $rows = $spreadsheet->getActiveSheet()->toArray();

                    $this->get('session')->remove('results');
                    unset($rows[0]);
                    unset($rows[1]);
                    unset($rows[2]);

                    foreach ($rows as $row) {
                        if ($row[9] != "") {
                                $section = $this->getDoctrine()
                                    ->getRepository(Section::class)
                                    ->findOneByNoClientOrName(trim($row[9]));
                                if ($section != null) {
                                    $results[] = $section->getNoClient();
                                } else {
                                    $results[] = trim($row[9]);
                                }
                            }
                        }

                    $results = array_unique($results);

                    $this->get('session')->set('results', $results);

                    return $this->redirectToRoute("app_count_list");
                } catch (FileException $e) {
                    // ... handle exception if something happens during file upload
                }
            }
        }

        return $this->render(
            'count/import.html.twig',
            [
            'form' => $form->createView(),
            ]
        );
    }

    /**
     * @Route("/count/export")
     */
    public function export(Request $request)
    {
        {
            $entityManager = $this->getDoctrine()->getManager();

            $form = $this->createForm(ImportSectionType::class);
            $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /**
            *
            *
            * @var UploadedFile $file
            */
            $uploadedFile = $form->get('file')->getData();

            if ($uploadedFile) {
                try {
                    $spreadsheet = PhpSpreadsheet\IOFactory::load($uploadedFile);
                    $rows = $spreadsheet->getActiveSheet()->toArray();
                    $headers[] = array();
                    $offset = 9;

                    // headers
                    for ($i=0; $i<=2; $i++) {
                        $array =  ($i == 2 ? array("Région","Fédération") : array("","")) ;
                        $headers[] = array_merge($array, array_slice($rows[$i], $offset));
                        unset($rows[$i]);
                    }

                    foreach ($rows as $row) {
                        $section =  $this->getDoctrine()
                            ->getRepository(Section::class)
                            ->findOneByNoClientOrName(trim($row[9]));

                        if ($section != null && $section->getFederation() != null) {
                            $output[] = array_merge(
                                array($section->getFederation()->getRegion()->getLibelle()),
                                array($section->getFederation()->getLibelle()),
                                array_slice($row, $offset)
                            );
                        }
                    }

                    $region  = array_column($output, 0);
                    $section = array_column($output, 1);
                    $federation = array_column($output, 2);
                    array_multisort($region, SORT_ASC, $section, SORT_ASC, $federation, SORT_ASC, $output);

                    $output = array_merge($headers, $output);

                    $spreadsheet = new PhpSpreadsheet\Spreadsheet();

                    $sheet = $spreadsheet->getActiveSheet();

                    $sheet->setTitle('User List');

                    $sheet->fromArray($output, null, 'A1', true);


                    $writer = new Xlsx($spreadsheet);

                    $writer->save('Export classé des réponses Permanences 2020 - 20210201.xlsx');
                    // TODO : change to current date.
                } catch (FileException $e) {
                    // ... handle exception if something happens during file upload
                }
            }
        }

            return $this->render(
                'count/import.html.twig',
                [
                'form' => $form->createView(),
                ]
            );
        }
    }

    /**
     * @Route("/bureau/import")
     */
    public function bureauImport(Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();

        $form = $this->createForm(ImportSectionType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /**
            *
            *
            * @var UploadedFile $file
            */
            $uploadedFile = $form->get('file')->getData();

            if ($uploadedFile) {
                try {
                    $spreadsheet = PhpSpreadsheet\IOFactory::load($uploadedFile);
                    $rows = $spreadsheet->getActiveSheet()->toArray();

                    unset($rows[0]);

                    foreach ($rows as $row) {
                        $region = $this->getDoctrine()
                            ->getRepository(Region::class)
                            ->findOneByNoClientOrName(trim($row[1]));
                        $federation = $this->getDoctrine()
                            ->getRepository(Federation::class)
                            ->findOneByNoClientOrName(trim($row[2]));
                        $section = $this->getDoctrine()
                            ->getRepository(Section::class)
                            ->findOneByNoClientOrName(trim($row[3]));
                        $person = $this->getDoctrine()
                            ->getRepository(Person::class)
                            ->findOneByDenominationAndEmail($row[5], $row[7], $row[6], $row[8]);

                        // Binding 1 federation to its region
                        if ($region != null && $federation != null) {
                            $federation->setRegion($region);
                            $entityManager->persist($federation);
                        }

                        // Binding 1 section to its federation
                        if ($federation != null && $section != null) {
                            $section->setFederation($federation);
                            $entityManager->persist($section);
                        }

                        // Binding 1 section to its members
                        if ($section != null && $person != null) {
                            switch (trim($row[4])) {
                                case "a pour président":
                                    $section->setPresident($person);
                                    break;
                                case "a pour Trésorier":
                                    $section->setTreasurer($person);
                                    break;
                                case "a pour Secrétaire":
                                    $section->setSecretary($person);
                                    break;
                            }

                            $entityManager->persist($section);
                        }

                        // Binding 1 region to its members
                        //if($section != null && $region != null && $section->getLibelle() == $region->getLibelle() ) {
                        if ($row[1] == $row[3]) {
                            switch (trim($row[4])) {
                                case "a pour Délégué Régional":
                                    $region->setDelegueRegional($person);
                                    break;
                                case "a pour Trésorier":
                                    $region->setTreasurer($person);
                                    break;
                                case "a pour Secrétaire":
                                    $region->setSecretary($person);
                                    break;
                            }

                            $entityManager->persist($region);
                        }
                    }

                    $entityManager->flush();

                    //return $this->redirectToRoute("app_section_list");
                } catch (FileException $e) {
                    // ... handle exception if something happens during file upload
                }
            }
        }

        return $this->render(
            'count/import.html.twig',
            [
            'form' => $form->createView(),
            ]
        );
    }
}
