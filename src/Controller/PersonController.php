<?php

namespace App\Controller;

use App\Entity\Section;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use App\Form\ImportCountType;
use App\Entity\Person;
use PhpOffice\PhpSpreadsheet;

class PersonController extends AbstractController
{
    /**
     * @Route("/count")
     */
    public function home(Request $request) //, SluggerInterface $slugger)
    {
        return $this->redirectToRoute("app_person_list");
    }

    /**
     * @Route("/person/list")
     */
    public function list(Request $request)
    {
        $persons = $this->getDoctrine()
            ->getRepository(Person::class)
            ->findAll();

        if ($persons == null) {
            return $this->redirectToRoute("app_person_import");
        }

        return $this->render(
            'person/list.html.twig',
            [
            'persons' => $persons
            ]
        );
    }

    /**
     * @Route("/person/import")
     */
    public function import(Request $request) //, SluggerInterface $slugger)
    {
        $entityManager = $this->getDoctrine()->getManager();

        $form = $this->createForm(ImportCountType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /**
*
             *
 * @var UploadedFile $file
*/
            $uploadedFile = $form->get('file')->getData();

            if ($uploadedFile) {
                try {
                    $spreadsheet = PhpSpreadsheet\IOFactory::load($uploadedFile);
                    $rows = $spreadsheet->getActiveSheet()->toArray();

                    unset($rows[0]);

                    foreach ($rows as $row) {
                        $person = $this->getDoctrine()
                            ->getRepository(Person::class)
                            ->findOneByEmail($row[8]);

                        if ($person == null) {
                            $person = new Person();
                        }

                        $person->setTitle($row[5]);
                        $person->setFirstName($row[7]);
                        $person->setLastName($row[6]);
                        $person->setEmail($row[8]);
                        $person->setTelephoneHome($row[9]);

                        $entityManager->persist($person);
                        $entityManager->flush();
                    }

                    return $this->redirectToRoute("app_person_list");
                } catch (FileException $e) {
                    // ... handle exception if something happens during file upload
                }
            }
        }

        return $this->render(
            'count/import.html.twig',
            [
            'form' => $form->createView(),
            ]
        );
    }
}
