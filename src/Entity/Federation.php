<?php

namespace App\Entity;

use App\Repository\FederationRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=FederationRepository::class)
 */
class Federation
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $libelle;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $noClient;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $compte;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $preferedEmailaddress;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $address;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $zipCode;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $town;

    /**
     * @ORM\OneToOne(targetEntity=Person::class, inversedBy="federationPresident", cascade={"persist", "remove"})
     */
    private $president;

    /**
     * @ORM\OneToOne(targetEntity=Person::class, inversedBy="federationTreasurer", cascade={"persist", "remove"})
     */
    private $treasurer;

    /**
     * @ORM\OneToOne(targetEntity=Person::class, inversedBy="federationSecretary",cascade={"persist", "remove"})
     */
    private $secretary;

    /**
     * @ORM\ManyToOne(targetEntity=Region::class, inversedBy="federations")
     */
    private $region;

    /**
     * @ORM\OneToMany(targetEntity=Section::class, mappedBy="federation")
     */
    private $sections;

    public function __construct()
    {
        $this->sections = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    public function setLibelle(string $libelle): self
    {
        $this->libelle = $libelle;

        return $this;
    }

    public function getNoClient(): ?string
    {
        return $this->noClient;
    }

    public function setNoClient(string $noClient): self
    {
        $this->noClient = $noClient;

        return $this;
    }

    public function getCompte(): ?string
    {
        return $this->compte;
    }

    public function setCompte(?string $compte): self
    {
        $this->compte = $compte;

        return $this;
    }

    public function getPreferedEmailaddress(): ?string
    {
        return $this->preferedEmailaddress;
    }

    public function setPreferedEmailaddress(?string $preferedEmailaddress): self
    {
        $this->preferedEmailaddress = $preferedEmailaddress;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(?string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getZipCode(): ?string
    {
        return $this->zipCode;
    }

    public function setZipCode(?string $zipCode): self
    {
        $this->zipCode = $zipCode;

        return $this;
    }

    public function getTown(): ?string
    {
        return $this->town;
    }

    public function setTown(?string $town): self
    {
        $this->town = $town;

        return $this;
    }

    public function getPresident(): ?Person
    {
        return $this->president;
    }

    public function setPresident(?Person $president): self
    {
        $this->president = $president;

        return $this;
    }

    public function getTreasurer(): ?Person
    {
        return $this->treasurer;
    }

    public function setTreasurer(?Person $treasurer): self
    {
        $this->treasurer = $treasurer;

        return $this;
    }

    public function getSecretary(): ?Person
    {
        return $this->secretary;
    }

    public function setSecretary(?Person $secretary): self
    {
        $this->secretary = $secretary;

        return $this;
    }

    public function getRegion(): ?Region
    {
        return $this->region;
    }

    public function setRegion(?Region $region): self
    {
        $this->region = $region;

        return $this;
    }

    /**
     * @return Collection|Section[]
     */
    public function getSections(): Collection
    {
        return $this->sections;
    }

    public function addSection(Section $section): self
    {
        if (!$this->sections->contains($section)) {
            $this->sections[] = $section;
            $section->setFederation($this);
        }

        return $this;
    }

    public function removeSection(Section $section): self
    {
        if ($this->sections->removeElement($section)) {
            // set the owning side to null (unless already changed)
            if ($section->getFederation() === $this) {
                $section->setFederation(null);
            }
        }

        return $this;
    }
}
